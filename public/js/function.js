//add item to cart
function addToCart1() {
    var img1 = document.getElementById('item_img1').src
    var name1 = document.getElementById('item_name1').innerHTML
    var price1 = document.getElementById('item_price1').innerHTML
    var qty1 = document.getElementById('item_qty1').value

    localStorage.setItem('img1', img1)
    localStorage.setItem('name1', name1)
    localStorage.setItem('price1', price1)
    localStorage.setItem('qty1', qty1)

    alert(name1 + ' x' + qty1 + ' added to the cart.')
}

function addToCart2() {
    var img2 = document.getElementById('item_img2').src
    var name2 = document.getElementById('item_name2').innerHTML
    var price2 = document.getElementById('item_price2').innerHTML
    var qty2 = document.getElementById('item_qty2').value

    localStorage.setItem('img2', img2)
    localStorage.setItem('name2', name2)
    localStorage.setItem('price2', price2)
    localStorage.setItem('qty2', qty2)

    alert(name2 + ' x' + qty2 + ' added to the cart.')
}

function addToCart3() {
    var img3 = document.getElementById('item_img3').src
    var name3 = document.getElementById('item_name3').innerHTML
    var price3 = document.getElementById('item_price3').innerHTML
    var qty3 = document.getElementById('item_qty3').value

    localStorage.setItem('img3', img3)
    localStorage.setItem('name3', name3)
    localStorage.setItem('price3', price3)
    localStorage.setItem('qty3', qty3)


    alert(name3 + ' x' + qty3 + ' added to the cart.')
}

//string to numbers
var qty1_val = Number(localStorage.getItem('qty1'))
var qty2_val = Number(localStorage.getItem('qty2'))
var qty3_val = Number(localStorage.getItem('qty3'))

var price1_val = Number(localStorage.getItem('price1'))
var price2_val = Number(localStorage.getItem('price2'))
var price3_val = Number(localStorage.getItem('price3'))

//display added item to cart
    document.getElementById('img_cart1').src = localStorage.getItem('img1')
    document.getElementById('name_cart1').innerHTML = localStorage.getItem('name1')
    document.getElementById('price_cart1').innerHTML = localStorage.getItem('price1')
    document.getElementById('qty_cart1').innerHTML = qty1_val 
    
    document.getElementById('img_cart2').src = localStorage.getItem('img2')
    document.getElementById('name_cart2').innerHTML = localStorage.getItem('name2')
    document.getElementById('price_cart2').innerHTML = localStorage.getItem('price2')
    document.getElementById('qty_cart2').innerHTML = qty2_val

    document.getElementById('img_cart3').src = localStorage.getItem('img3')
    document.getElementById('name_cart3').innerHTML = localStorage.getItem('name3')
    document.getElementById('price_cart3').innerHTML = localStorage.getItem('price3')
    document.getElementById('qty_cart3').innerHTML = qty3_val

//hide div
if (qty1_val === 0 && qty2_val === 0 && qty3_val === 0) {
    document.getElementById("hide_1").style.display = "none";
    document.getElementById("hide_2").style.display = "none";
    document.getElementById("hide_3").style.display = "none";
}else if (qty1_val === 0 && qty2_val === 0) {
    document.getElementById("hide_1").style.display = "none";
    document.getElementById("hide_2").style.display = "none";
}else if (qty1_val === 0 && qty3_val === 0) {
    document.getElementById("hide_1").style.display = "none";
    document.getElementById("hide_3").style.display = "none";
}else if (qty2_val === 0 && qty3_val === 0) {
    document.getElementById("hide_2").style.display = "none";
    document.getElementById("hide_3").style.display = "none";
}

//display total on the cart
qtyPriceTotal1 = qty1_val * price1_val
qtyPriceTotal2 = qty2_val * price2_val
qtyPriceTotal3 = qty3_val * price3_val

subTotal = qtyPriceTotal1 + qtyPriceTotal2 + qtyPriceTotal3
document.getElementById('qty_price1').innerHTML = qtyPriceTotal1
document.getElementById('qty_price2').innerHTML = qtyPriceTotal2
document.getElementById('qty_price3').innerHTML = qtyPriceTotal3
document.getElementById('total').innerHTML = subTotal

var shipFee_val = Number(document.getElementById('ship_fee').innerHTML)
//total + shipping fee
total = subTotal + shipFee_val
//total + shipping fee display
document.getElementById('totalFee').innerHTML = total

//continue shopping display qty
function continueShopping() {
document.getElementById('item_qty1').value = qty1_val
document.getElementById('item_qty2').value = qty2_val
document.getElementById('item_qty3').value = qty3_val
}

//delete cart item
function deleteItem1() {
    localStorage.removeItem('img1')
    localStorage.removeItem('name1')
    localStorage.removeItem('price1')
    localStorage.removeItem('qty1')
    alert('Item removed!')
    location.reload()
}

function deleteItem2() {
    localStorage.removeItem('img2')
    localStorage.removeItem('name2')
    localStorage.removeItem('price2')
    localStorage.removeItem('qty2')
    alert('Item removed!')
    location.reload()
}

function deleteItem3() {
    localStorage.removeItem('img3')
    localStorage.removeItem('name3')
    localStorage.removeItem('price3')
    localStorage.removeItem('qty3')
    alert('Item removed!')
    location.reload()
}

//empty cart
function emptyCart() {
    localStorage.clear()
    alert('Cart Empty!')
    location.reload()
}

//shop now + clear storage
function clearShop() {
    localStorage.clear()
    window.location.href = 'index.html';
}

//place order
function placeOrder() {
    var bill_name = document.getElementById('bill_name').value
    var bill_number = document.getElementById('bill_number').value
    var bill_email = document.getElementById('bill_email').value
    var bill_address = document.getElementById('bill_address').value

    localStorage.setItem('bill_name', bill_name)
    localStorage.setItem('bill_number', bill_number)
    localStorage.setItem('bill_email', bill_email)
    localStorage.setItem('bill_address', bill_address)

    var ship_name = document.getElementById('ship_name').value
    var ship_number = document.getElementById('ship_number').value
    var ship_email = document.getElementById('ship_email').value
    var ship_address = document.getElementById('ship_address').value

    localStorage.setItem('ship_name', ship_name)
    localStorage.setItem('ship_number', ship_number)
    localStorage.setItem('ship_email', ship_email)
    localStorage.setItem('ship_address', ship_address)

    window.location.href = 'order.html';
}

function messageConfirmation() {
    alert('Your order has been placed!')
}

//display order
document.getElementById('order_total').innerHTML = total
document.getElementById('order_bname').innerHTML = localStorage.getItem('bill_name')
document.getElementById('order_bnumber').innerHTML = localStorage.getItem('bill_number')
document.getElementById('order_bemail').innerHTML = localStorage.getItem('bill_email')
document.getElementById('order_baddress').innerHTML = localStorage.getItem('bill_address')

document.getElementById('order_sname').innerHTML = localStorage.getItem('ship_name')
document.getElementById('order_snumber').innerHTML = localStorage.getItem('ship_number')
document.getElementById('order_semail').innerHTML = localStorage.getItem('ship_email')
document.getElementById('order_saddress').innerHTML = localStorage.getItem('ship_address')

  